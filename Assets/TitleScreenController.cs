﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Facebook.Unity;
using UnityEngine.UI;

namespace UnityStandardAssets._2D
{
    public class TitleScreenController : MonoBehaviour
    {
		[SerializeField]
		GameObject m_startButton;
		[SerializeField]
		GameObject m_loadingText;


        private QuestionManager m_questionManager;
        private AudioManager m_audioManager;
        private SettingsManager m_settingsManager;
        private PictureManager m_thumbnailManager;
        private PlayerProgress m_playerProgress;


        // Awake function from Unity's MonoBehavior
        void Awake()
        {
            if (!FB.IsInitialized)
            {
                // Initialize the Facebook SDK
                FB.Init(InitCallback, OnHideUnity);
            }
            else
            {
                // Already initialized, signal an app activation App Event
                FB.ActivateApp();
            }
        }

        private void InitCallback()
        {
            if (FB.IsInitialized)
            {
                // Signal an app activation App Event
                FB.ActivateApp();
                // Continue with Facebook SDK
                // ...
            }
            else
            {
                Debug.Log("Failed to Initialize the Facebook SDK");
            }
        }

        private void OnHideUnity(bool isGameShown)
        {
            if (!isGameShown)
            {
                // Pause the game - we will need to hide
                Time.timeScale = 0;
            }
            else
            {
                // Resume the game - we're getting focus again
                Time.timeScale = 1;
            }
        }

        // Use this for initialization
        void Start()
        {
            m_startButton.SetActive(false);

            m_questionManager = QuestionManager.GetQuestionManager();
            m_audioManager = AudioManager.GetAudioManager();
            m_settingsManager = SettingsManager.GetSettingsManager();
            m_thumbnailManager = PictureManager.GetThumbnailManager();

            m_settingsManager.ResetSettings();

            m_questionManager.LoadQuestions(LoadThumbnails);
        }

        void LoadThumbnails()
        {
            m_playerProgress = PlayerProgress.GetPlayerProgress();

            // load all the thumbnails
            string[] thumbnails = m_questionManager.AllThumbnailNamesInAllQuizzes();
            CallBack finishedLoadingTexturesCallback = FinishedLoadingTextures;
            m_thumbnailManager.LoadAllSpecifiedTextures(thumbnails, "", FinishedLoadingTextures);
        }

        void FinishedLoadingTextures()
        {
			m_loadingText.SetActive (false);
            m_startButton.SetActive(true);
        }

        // Update is called once per frame
        void Update()
        {

        }

        public void StartButtonPressed()
        {
            Debug.Log("pressed start button");

			m_audioManager.PlayAudioClip ("start");

            PlayerProgress playerProgress = PlayerProgress.GetPlayerProgress();
			if (false)	//playerProgress.TotalPlayerScore() == 0)
            {
                // player is completely new. Go straight into the game with the first quiz.
                QuestionManager qm = QuestionManager.GetQuestionManager();
                qm.SetQuiz(0, 0);
                string sceneName = qm.GetQuizSceneNameForCurrentMode();
                Debug.Log("scene name : "+sceneName);

                SceneManager.LoadScene(sceneName);
            }
            else
            {
                // player has played the game before. Go into level select.
                SceneManager.LoadScene("jigsaw Level Select");
            }
        }

        public void ShareButtonPressed(string FB_URL)
        {
            Application.OpenURL(FB_URL);
        }
		
		public void RateButtonPressed()
		{
#if UNITY_ANDROID
            Application.OpenURL("market://details?id=" + "gopencils.games.fine.art.jigsaw");
#elif UNITY_IOS
            Application.OpenURL("itms-apps://itunes.apple.com/app/id" + "1336361631");
#endif
		}
    }
}
