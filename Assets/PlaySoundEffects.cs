﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundEffects : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlaySoundEffect(string name)
    {
        UnityStandardAssets._2D.AudioManager audioManager = UnityStandardAssets._2D.AudioManager.GetAudioManager();
        if (audioManager != null)
        {
            audioManager.PlayAudioClip(name);
        }
    }
}
